mod utils;
mod intvm;
use std::fs;

const INPUT_FILE: &str = "input.txt";

fn main() {
    let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
    //let file = &String::from("3,0,4,0,99");
    let input = get_input(file);
    println!("Result Part 1: {}", part_01(&input));
    println!("Result Part 2: {}", part_02(&input));
}

fn get_input(input: &String) -> Vec<&str> {
    let codes = input.split(",");
    codes.collect::<Vec<&str>>()
}

fn part_01(input: &Vec<&str>) -> i64{

    let mut vm = intvm::create_vm(input);

    vm.input.push_back(1);
    vm.print_output = true;
    vm.run();
    
    vm.output.pop_back().expect("No output written")
}

#[allow(unused_variables)]
fn part_02(input: &Vec<&str>) -> i64{

    let mut vm = intvm::create_vm(input);

    vm.input.push_back(5);

    vm.run();
    
    vm.output.pop_back().expect("No output written")
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part01()  {
        let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
        let input = get_input(file);
    
        assert_eq!(part_01(&input), 15314507);
    }

    #[test]
    fn test_part02()  {
        let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
        let input = get_input(file);
    
        assert_eq!(part_02(&input), 652726);
    }

    #[test]
    fn test_part02_example_lage() {
        let file = &String::from("3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99");
        let input = get_input(file);
    

        let mut vm = intvm::create_vm(&input);
        vm.input.push_back(6);
        vm.run();

        assert_eq!(
            vm.output.pop_back().expect("No output written"),
            999
        );
    }
}