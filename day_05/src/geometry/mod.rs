#[derive(Debug)]
pub struct Point {
    pub x: f64,
    pub y: f64,
}

impl Point {
    pub fn manhattan(&self, p: &Self) -> f64 {
        ((self.x-p.x).abs() + (self.y - p.y).abs())
    }

    pub fn distance(&self, p: &Self) -> f64 {
        ((self.x - p.x).powi(2) + (self.y - p.y).powi(2)).sqrt()
    }
}

#[derive(Debug)]
pub struct Line {
    pub a: Point,
    pub b: Point,
}

impl Line {
    pub fn get_intersection(&self, other: &Self) -> Option<Point> {
        let a1 = self.a.y - self.b.y;
        let b1 = self.a.x - self.b.x;
        let c1 = a1 * self.a.x + b1 * self.a.y;
 
        let a2 = other.b.y - other.a.y;
        let b2 = other.a.x - other.b.x;
        let c2 = a2 * other.a.x + b2 * other.a.y;
 
        let delta = a1 * b2 - a2 * b1;
 
        //println!("{}", delta);

        if delta == 0.0 {
            return None;
        }
 
    
        //println!("{:?} & {:?}", self, other);
        let point = Point {
            x: (b2 * c1 - b1 * c2) / delta,
            y: (a1 * c2 - a2 * c1) / delta,
        };

        if !self.contains_point(&point) || !other.contains_point(&point) {
            return None;
        }

        Some(point)
    }

    pub fn contains_point(&self, other: &Point) -> bool {       
        self.a.manhattan(other) + other.manhattan(&self.b) == self.a.manhattan(&self.b)
    }

    pub fn len(&self) -> f64 {
        self.a.manhattan(&self.b)
    }
}