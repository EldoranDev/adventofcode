mod utils;
mod intvm;
mod geometry;

use std::fs;

const INPUT_FILE: &str = "input.txt";

fn main() {
    let input = fs::read_to_string(INPUT_FILE).expect("Error reading input");
    //let input = "R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83";
    //let input = "R8,U5,L5,D3\nU7,R6,D4,L4";
    let input = input.lines();
    let input: Vec<&str> = input.collect::<Vec<&str>>();

    part_01(input.clone());
    part_02(input.clone());
}

fn part_01(input: Vec<&str>) {
    let first_cable = get_lines(input[0]);
    let second_cable = get_lines(input[1]);

    let mut intersections: Vec<geometry::Point> = Vec::new();

    for line in first_cable {
        for target_line in &second_cable {
            let intersection = line.get_intersection(target_line);

            match intersection {
                Some(x) => {
                    if x.x != 0.0 && x.y != 0.0 {
                        intersections.push(x);
                    }
                },
                None => continue,
            }
        }
    }
    
    let mut shortest: f64 = 10000000000000000.0;
    let origin = geometry::Point{ x: 0.0, y: 0.0};

    for point in intersections {
        let distance = point.manhattan(&origin);

        if distance < shortest {
            shortest = distance
        }
    }

    println!("Result Part 1: {}", shortest);
}

fn part_02(input: Vec<&str>) {
    let first_cable = get_lines(input[0]);
    let second_cable = get_lines(input[1]);

    let mut a = 0.0;
    let mut b: f64;

    let mut shortest = 1000000000000000000.0;

    for line in first_cable {
        b = 0.0;

        for target_line in &second_cable {
            let intersection = line.get_intersection(target_line);

            if intersection.is_some() {
                let x = intersection.unwrap();
                    if x.x != 0.0 && x.y != 0.0 {
                        let ta = a + line.a.manhattan(&x);
                        let tb = b + target_line.a.manhattan(&x);

                        if ta + tb < shortest {
                            shortest = ta + tb;
                        }
                    }
            }
            b = b + target_line.len();
        }
        a = a + line.len();
    }

    println!("Result Part 2: {}", shortest);
    
}


fn get_lines(input: &str) -> Vec<geometry::Line> {
    let path = input.split(",");
    let path: Vec<&str> = path.collect::<Vec<&str>>();

    let mut pos: geometry::Point = geometry::Point{ x: 0.0, y: 0.0};
    let mut lines: Vec<geometry::Line> = Vec::new();

    for p in path {        
        let parts = p.split_at(1);
        let direction = parts.0;
        let length: f64 = parts.1.parse().expect("error parsing input");
        let mut target = geometry::Point{ x: pos.x, y: pos.y};

        match direction {
            "U" => {
                target.y = target.y + length;
            },
            "R" => {
                target.x = target.x + length;
            },
            "L" => {
                target.x = target.x - length;
            },
            "D" => {
                target.y = target.y - length;
            },
            _ => panic!("unknown direction: {}", direction),
        }
        
        lines.push(geometry::Line {
            a: geometry::Point {
                x: pos.x,
                y: pos.y,
            },

            b: geometry::Point {
                x: target.x,
                y: target.y,
            }
        });

        pos.x = target.x;
        pos.y = target.y;
    }

    lines
}