mod utils;
mod intvm;

use intvm::IntVM;
use std::fs;
use std::collections::VecDeque;
use permutohedron::LexicalPermutation;

const INPUT_FILE: &str = "input.txt";

fn main() {
    let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");

    let input = get_input(file);
    println!("Result Part 1: {}", part_01(&input));
    println!("Result Part 2: {}", part_02(&input));
}

fn get_input(input: &String) -> Vec<&str> {
    let codes = input.split(",");
    codes.collect::<Vec<&str>>()
}

fn part_01(input: &Vec<&str>) -> i64{   
    let mut data = [0, 1, 2, 3, 4];
    let mut permutations = Vec::new();

    loop {
        permutations.push(data.to_vec());
        if !data.next_permutation() {
            break;
        }
    }
    
    let mut highest = 0;

    for phase in permutations {
        let mut p = VecDeque::from(phase);
        let mut network = create_network(input, &mut p);
 
        let res = network.run();

        if res > highest {
            highest = res;
        }
    }

    highest
}


fn part_02(input: &Vec<&str>) -> i64{
    let mut data = [5, 6, 7, 8, 9];
    let mut permutations = Vec::new();

    loop {
        permutations.push(data.to_vec());
        if !data.next_permutation() {
            break;
        }
    }

    let mut highest = 0;

    for phase in permutations {
        let mut p = VecDeque::from(phase);
        let mut network = create_network(input, &mut p);
 
        let res = network.run();

        if res > highest {
            highest = res;
        }
    }

    highest
}

struct TrusterNetwork {
    a: IntVM,
    b: IntVM,
    c: IntVM,
    d: IntVM,
    e: IntVM,
}

impl TrusterNetwork {
    fn run(&mut self) -> i64 {

        let mut last_output = 0;

        while !self.e.is_halted {
            self.a.input.push_back(last_output);
            self.a.run();

            last_output = self.a.output.pop_back().unwrap();

            self.b.input.push_back(last_output);
            self.b.run();

            last_output = self.b.output.pop_back().unwrap();

            self.c.input.push_back(last_output);
            self.c.run();

            last_output = self.c.output.pop_back().unwrap();

            self.d.input.push_back(last_output);
            self.d.run();
            
            last_output = self.d.output.pop_back().unwrap();

            self.e.input.push_back(last_output);
            self.e.run();

            last_output = self.e.output.pop_back().unwrap();
        }

        last_output
    }
}

fn create_network(input: &Vec<&str>, phases: &mut VecDeque<i64>) -> TrusterNetwork {

    let mut a = intvm::create_vm(input);
    let mut b = intvm::create_vm(input);
    let mut c = intvm::create_vm(input);
    let mut d = intvm::create_vm(input);
    let mut e = intvm::create_vm(input);

    a.input.push_back(phases.pop_front().unwrap());
    b.input.push_back(phases.pop_front().unwrap());
    c.input.push_back(phases.pop_front().unwrap());
    d.input.push_back(phases.pop_front().unwrap());
    e.input.push_back(phases.pop_front().unwrap());

    let network = TrusterNetwork {
        a,
        b,
        c,
        d,
        e,
    };

    network
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part01() {
        let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");

        let input = get_input(file);

        assert_eq!(
            part_01(&input),
            273814
        );
    }

    #[test]
    fn test_part01_1st() {
        let file = &String::from("3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0");
        let input = get_input(file);
        let mut phases = VecDeque::from(vec![4, 3, 2, 1, 0]);
        
        let mut network = create_network(&input, &mut phases);


        assert_eq!(
            network.run(), 
            43210
        );
    }

    #[test]
    fn test_part01_2nd() {
        let file = &String::from("3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0");
        let input = get_input(file);
        let mut phases = VecDeque::from(vec![0, 1, 2, 3, 4]);
        
        let mut network = create_network(&input, &mut phases);
        assert_eq!(
            network.run(), 
            54321
        );
    }

    #[test]
    fn test_part01_3rd() {
        let file = &String::from("3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0");
        let input = get_input(file);
        let mut phases = VecDeque::from(vec![1,0,4,3,2]);

        let mut network = create_network(&input, &mut phases);
        assert_eq!(
            network.run(), 
            65210
        );
    }

    #[test]
    fn test_part02() {
        let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");

        let input = get_input(file);

        assert_eq!(
            part_02(&input),
            34579864
        );
    }


    #[test]
    fn test_part02_1st() {
        let file = &String::from("3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5");
        let input = get_input(file);
        let mut phases = VecDeque::from(vec![9,8,7,6,5]);

        let mut network = create_network(&input, &mut phases);
        
        assert_eq!(
            network.run(), 
            139629729
        );
    }

    #[test]
    fn test_part02_2nd() {
        let file = &String::from("3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10");
        let input = get_input(file);
        let mut phases = VecDeque::from(vec![9,7,8,5,6]);

        let mut network = create_network(&input, &mut phases);
        assert_eq!(
            network.run(), 
            18216
        );
    }
}