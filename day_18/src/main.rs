mod utils;
mod intvm;
mod map;

use std::fs;
use std::iter::FromIterator;
use std::cmp::Ordering;
use std::collections::{HashMap, BinaryHeap};
use map::{Pos, Path, Key};
use pathfinding::directed::astar;

const INPUT_FILE_ONE: &str = "input-1.txt";
const INPUT_FILE_TWO: &str = "input-2.txt";

fn main() {
    let input_one = fs::read_to_string(INPUT_FILE_ONE).expect("Error reading input");


    let input_one = input_one.lines();
    let input_one: Vec<&str> = input_one.collect::<Vec<&str>>();
    
    let input_two = fs::read_to_string(INPUT_FILE_TWO).expect("Error reading input");
    let input_two = input_two.lines();
    let input_two: Vec<&str> = input_two.collect::<Vec<&str>>();

    println!("Result Part 1: {}", part_01(input_one.clone()));
    //println!("Result Part 2: {}", part_02(input_two.clone()));
}

type Map = HashMap<Pos, char>;
type Network = HashMap<char, Key>;

#[derive(Clone, Eq, Debug, PartialEq)]
struct SingleState(char, Vec<char>, u32, Vec<char>);

#[derive(Clone, Eq, Debug, PartialEq)]
struct MultiState(Vec<char>, Vec<char>, u32, Vec<char>);

impl Ord for SingleState {
    fn cmp(&self, other: &SingleState) -> Ordering {
        other.2.cmp(&self.2).then_with(|| other.1.len().cmp(&self.1.len()))
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for SingleState {
    fn partial_cmp(&self, other: &SingleState) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

impl Ord for MultiState {
    fn cmp(&self, other: &MultiState) -> Ordering {
        other.2.cmp(&self.2).then_with(|| other.1.len().cmp(&self.1.len()))
    }
}

// `PartialOrd` needs to be implemented as well.
impl PartialOrd for MultiState {
    fn partial_cmp(&self, other: &MultiState) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

#[allow(unused_variables)]
fn part_01(input: Vec<&str>) -> u32 {
    let mut map: Map = HashMap::new();

    let mut x = 0;
    let mut y = 0;

    let mut keys: Vec<Pos> = Vec::new();
    let mut needed_keys: Vec<char> = Vec::new();

    for line in input {
        for c in line.chars() {
            map.insert(Pos(x, y), c);
            
            print!("{}", c);

            match c as u8 {
                48 ..= 57 => {
                    keys.push(Pos(x, y));
                    
                },
                96 ..= 122 => {
                    keys.push(Pos(x, y));
                    needed_keys.push(c);
                },
                64 => keys.push(Pos(x, y)),
                _ => (),
            };

            x += 1;
        }
        println!();
        y += 1;
        x = 0;
    }

    let network = build_network(&map,&keys);
    let mut queue: BinaryHeap<SingleState> = BinaryHeap::new();

    println!("{:?}", needed_keys);
    println!("{:?}", network.get(&'a').unwrap());

    queue.push(SingleState('@', needed_keys, 0, Vec::new()));
    
    let mut cache: HashMap<String, u32> = HashMap::new();

    while queue.len() > 0 {
        let current = queue.pop().unwrap();
        
        let hash = String::from_iter(&current.3);

        if let Some(val) = cache.get(&hash) {
            if *val < current.2 {
                //continue;
            }
        }

        if current.1.len() == 0 {
            println!("{:?}", queue);
            return current.2;
        }

        for key in &current.1 {
            if let Some(path) = get_path_to_key(network.get(&current.0).unwrap(), *key, &current.1) {
                let mut new_visit = current.3.clone();
                let mut new_needed: Vec<char> = current.1.clone();
                
                let new_key = key.clone();
                let new_depth = path.len + current.2;

                new_visit.push(new_key);
                new_visit.sort();
                
                let hash = String::from_iter(&new_visit);

                if let Some(c) = cache.get_mut(&hash) {
                    if *c > new_depth {
                        println!("UPDATE {}: {} -> {}", hash, c, new_depth);
                        *c = new_depth;

                        new_needed.remove(new_needed.iter().position(|x| *x == new_key).unwrap());
                        //println!("UPDATE: {} -> {}", hash, new_depth);
                        queue.push(SingleState(new_key, new_needed, new_depth, new_visit));
                    }
                } else {
                    new_needed.remove(new_needed.iter().position(|x| *x == new_key).unwrap());
                    //println!("INSERT: {} -> {}", hash, new_depth);
                    cache.insert(hash, new_depth);
                    queue.push(SingleState(new_key, new_needed, new_depth, new_visit));
                }                
            }
        }
    }

    0
}

fn get_path_to_key(key: &Key, target: char, still_needed: &Vec<char>) -> Option<Path> {
    if let Some(path) = key.links.get(&target) {
        for gate in &path.gates {
            if still_needed.contains(&(((*gate as u8) + 32) as char)) {
                return None;
            }
        }

        return Some(path.clone());
    }

    None
}

fn build_network(map: &Map, keys: &Vec<Pos>) -> Network {
    let mut network: Network = Network::new();
    let successors = |p: &Pos| {
        p.successors()
            .iter()
            .filter(|f| {
                let pp = map.get(&f.0);
                match pp {
                    Some(x) => {
                        match x {
                            '#' => false,
                            _ => true,
                        }
                    },
                    None => {
                        false
                    }
                }
            })
            .map(|pp| pp.clone())
            .collect::<Vec<(Pos, u32)>>()
    };

    for from in keys {
        let from_key: char = *map.get(&from).unwrap();
        let mut key = Key {
            pos: from.clone(),
            key: from_key,
            links: HashMap::new(),
        };

        for to in keys {
            if from == to {
                continue;
            }
            let to_char = *map.get(&to).unwrap();
            let result = astar::astar(
                from,
                successors, 
                |p| p.distance(to),
                |p| *p == *to
            );

            match result {
                None => (),
                Some(path) => {
                    let mut p = Path {
                        from: from.clone(),
                        to: to.clone(),
                        len: (path.0.len() - 1) as u32,
                        gates: Vec::new(),
                    };

                    for pos in &path.0 {
                        let c = map.get(&pos).unwrap();
                        if *c >= 'A' && *c <= 'Z' {
                            p.gates.push(*c);
                        }
                    }

                    key.links.insert(to_char, p);
                },
            };
        }

        network.insert(from_key, key);
    }

    network
}

/*
#[allow(unused_variables)]
fn part_02(input: Vec<&str>) -> u32 {
    let mut map: Map = HashMap::new();

    let mut x = 0;
    let mut y = 0;

    let mut keys: Vec<Pos> = Vec::new();
    let mut needed_keys: Vec<char> = Vec::new();

    for line in input {
        for c in line.chars() {
            map.insert(Pos(x, y), c);

            match c {
                '1' ..= '4' => {
                    keys.push(Pos(x, y));
                },
                'a' ..= 'z' => {
                    keys.push(Pos(x, y));
                    needed_keys.push(c);
                },
                '@' => panic!("Part 2 should not contain an @ check input"),
                _ => (),
            };

            x += 1;
        }

        y += 1;
        x = 0;
    }

    let network = build_network(&map, &keys);
    let mut queue: BinaryHeap<MultiState> = BinaryHeap::new();

    let mut last = needed_keys.len();
    let mut cache: HashMap<String, u32> = HashMap::new();

    queue.push(MultiState(vec!['1', '2', '3', '4'], needed_keys, 0, Vec::new()));
    
    while queue.len() > 0 {
        let current = queue.pop().unwrap();
        let hash = String::from_iter(&current.3);

        if let Some(val) = cache.get(&hash) {
            if *val < current.2 {
                continue;
            }
        }

        if current.1.len() == 0 {
            return current.2;
        }

        if last > current.1.len() {
            last = current.1.len();
        }

        for bot_index in 0..current.0.len() {

            let bot = current.0.get(bot_index).unwrap();

            for key in &current.1 {
                if let Some(path) = get_path_to_key(network.get(bot).unwrap(), *key, &current.1) {
                    let mut new_visit = current.3.clone();
                    let new_key = key.clone();
                    let mut new_needed: Vec<char> = current.1.clone();
                    let new_depth = path.len + current.2 - 1;
                    let mut new_bots = current.0.clone();

                    new_bots.get_mut(bot_index).replace(&mut key.clone());
                    
                    new_visit.push(new_key);
                    new_visit.sort();
                    
                    let hash = String::from_iter(&new_visit);

                    if let Some(c) = cache.get_mut(&hash) {
                        if *c > new_depth {
                            *c = new_depth;
                        
                            new_needed.remove(new_needed.iter().position(|x| *x == new_key).unwrap());
                            //println!("UPDATE: {} -> {}", hash, new_depth);
                            queue.push(MultiState(new_bots, new_needed, new_depth, new_visit));
                        }
                    } else {
                        new_needed.remove(new_needed.iter().position(|x| *x == new_key).unwrap());
                        //println!("INSERT: {} -> {}", hash, new_depth);
                        cache.insert(hash, new_depth);
                        queue.push(MultiState(new_bots, new_needed, new_depth, new_visit));
                    }           
                }
            }
        }
    }

    0
}
*/