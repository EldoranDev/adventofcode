use std::collections::HashMap;

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Pos(pub i32, pub i32);

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Path {
    pub from: Pos,
    pub to: Pos,
    pub len: u32,
    pub gates: Vec<char>,
}

#[derive(Clone, Debug)]
pub struct Key {
    pub pos: Pos,
    pub key: char,
    pub links: HashMap<char, Path>,
}

impl Pos {
    pub fn successors(&self) -> Vec<(Pos, u32)> {
      let &Pos(x, y) = self;
      vec![Pos(x+1, y), Pos(x-1,y), Pos(x,y+1), Pos(x,y-1)]
        .into_iter().map(|p| (p, 1)).collect()
    }

    pub fn distance(&self, a: &Self) -> u32 {
        ((self.0 - a.0).abs() + (self.1 - a.1).abs()) as u32
    }
  }