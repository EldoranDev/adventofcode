mod utils;
mod intvm;

fn main() {
    part_01();
    part_02();
}

#[allow(unused_variables)]
fn part_01() {
    let mut count = 0;
    
    'outer: for pw in 183564..657474 {
        let s: String = pw.to_string();

        
        if is_valid(&s) {
            count = count+1;
        }
    }

    println!("Result part 1: {}", count);
}

fn is_valid(pw: &String) -> bool {
    let mut doubled = false;
    let mut last = 0;
    
    for c in pw.chars() {
        let num: i64 = (c.to_string()).parse().expect("error");
        
        if last > num {
            return false;
        } 

        if last == num {
            doubled = true;
        }
        
        last = num;
    }

    doubled
}

fn is_valid_new(pw: &String) -> bool {
    let mut doubled = false;
    let mut last = 0;

    let mut double_count = 0;
    
    for c in pw.chars() {
        let num: i64 = (c.to_string()).parse().expect("error");
        
        if last > num {
            return false;
        } 

        if last == num {
            double_count = double_count + 1;
        } else {
            if double_count == 1 {
                doubled = true;
            }

            double_count = 0;
        }
        
        last = num;
    }

    double_count == 1 || doubled
}

fn part_02() {
    let mut count = 0;

    'outer: for pw in 183564..657474 {
        let s: String = pw.to_string();

        
        if is_valid_new(&s) {
            count = count+1;
        }
    }

    println!("Result part 1: {}", count);
}
