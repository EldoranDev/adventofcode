use std::fs;
use math::round;
mod utils;

const INPUT_FILE: &str = "input.txt";

fn main() {
    let input = fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input = input.lines();
    let input: Vec<&str> = input.collect::<Vec<&str>>();

    part_01(input.clone());
    part_02(input.clone());
}

fn part_01(input: Vec<&str>) {
    let mut sum_fuel: i32 = 0 ;

    for mass in utils::vec_to_float(input) {
        sum_fuel = sum_fuel + get_fuel(mass);
    }

    println!("Overall needed: {}", sum_fuel);
}

fn part_02(input: Vec<&str>) {
    let mut sum_fuel: i32 = 0;

    for mass in utils::vec_to_float(input) {
        sum_fuel = sum_fuel + get_full_rec(mass);
    }

    println!("Overall needed rec: {}", sum_fuel);
}

fn get_full_rec(module: f64) -> i32 {
    if module  <= 0.0 {
        return 0 as i32
    }

    let needed_fuel = (round::floor(module / 3 as f64, 0) as i32) - 2;

    if needed_fuel < 0 {
        return 0 as i32
    }

    needed_fuel + get_full_rec(needed_fuel as f64)
}

fn get_fuel(module: f64) -> i32 {
    (round::floor(module / 3 as f64, 0) as i32) - 2
}