#[allow(dead_code)]
pub fn vec_to_int(input: Vec<&str>) -> Vec<i64> {
    input
    .into_iter()
    .map(|line| { line.parse().expect("erro reading input") })
    .collect()
}

#[allow(dead_code)]
pub fn vec_to_float(input: Vec<&str>) -> Vec<f64> {
    input
    .into_iter()
    .map(|line| { line.parse().expect("erro reading input") })
    .collect()
}

