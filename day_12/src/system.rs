use regex::Regex;
use crate::math::Index3;
use crate::moon::Moon;

const REGEX_PATTERN: &str = r"x=(-?\d*), y=(-?\d*), z=(-?\d*)";

pub struct System {
    pub moons: Vec<Moon>,
}

impl System {
    pub fn from(input: &Vec<&str>) -> System {
        let re = Regex::new(REGEX_PATTERN).unwrap();

        let mut sys = System {
            moons: Vec::new(),
        };

        for row in input {
            let capt = re.captures(row).unwrap();
            
            sys.moons.push(Moon {
                pos: Index3 {
                    x: capt.get(1).unwrap().as_str().parse::<i64>().unwrap(),
                    y: capt.get(2).unwrap().as_str().parse::<i64>().unwrap(),
                    z: capt.get(3).unwrap().as_str().parse::<i64>().unwrap(),
                },
                vel: Index3::zero(),
            });
        }

        sys
    }
    
    pub fn tick(&mut self) -> () {
        for moon in 0..self.moons.len() {
            for target in 0..self.moons.len() {
                if moon == target {
                    continue;
                }
                let target = self.moons[target].clone();

                if let Some(moon) = self.moons.get_mut(moon) {
                    (*moon).apply_gravity(&target);
                }
            }
        }

        for moon in &mut self.moons {
            moon.tick();
        }
    }

    pub fn get_x_hash(&self) -> String {
        let mut hash = String::new();

        for moon in &self.moons {
            hash.push_str(&moon.pos.x.to_string());
            hash.push_str(&moon.vel.x.to_string());
        }

        hash
    }

    pub fn get_y_hash(&self) -> String {
        let mut hash = String::new();

        for moon in &self.moons {
            hash.push_str(&moon.pos.y.to_string());
            hash.push_str(&moon.vel.y.to_string());
        }

        hash
    }
    pub fn get_z_hash(&self) -> String {
        let mut hash = String::new();

        for moon in &self.moons {
            hash.push_str(&moon.pos.z.to_string());
            hash.push_str(&moon.vel.z.to_string());
        }

        hash
    }
}