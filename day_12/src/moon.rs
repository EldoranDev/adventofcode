use std::fmt;
use crate::math::Index3;

#[derive(Debug, PartialEq, PartialOrd, Clone)]
pub struct Moon {
    pub pos: Index3,
    pub vel: Index3
}

impl fmt::Display for Moon {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "pos={}, vel={}", self.pos, self.vel)
    }
}

impl Moon {
    pub fn apply_gravity(&mut self, moon: &Moon) -> () {
        if self.pos.x > moon.pos.x {
            self.vel.x -= 1;
        }

        if self.pos.x < moon.pos.x {
            self.vel.x += 1;
        }

        if self.pos.y > moon.pos.y {
            self.vel.y -= 1;
        }

        if self.pos.y < moon.pos.y {
            self.vel.y += 1;
        }

        if self.pos.z > moon.pos.z {
            self.vel.z -= 1;
        }

        if self.pos.z < moon.pos.z {
            self.vel.z += 1;
        }
    }

    pub fn tick(&mut self) -> () {
        self.pos += self.vel.clone();
    }
}