mod utils;
mod intvm;
mod math;
mod moon;
mod system;

use moon::Moon;
use system::System;
use std::fs;
use math::Index3;
use regex::Regex;
use std::collections::HashSet;
use num::integer;

const INPUT_FILE: &str = "input.txt";
const REGEX_PATTERN: &str = r"x=(-?\d*), y=(-?\d*), z=(-?\d*)";



fn main() {
    let input = fs::read_to_string(INPUT_FILE).expect("Error reading input");
    /*let input = 
"<x=-8, y=-10, z=0>
<x=5, y=5, z=10>
<x=2, y=-7, z=3>
<x=9, y=-8, z=-3>";*/

    let input = input.lines();
    let input: Vec<&str> = input.collect::<Vec<&str>>();

    println!("Result part 01: {}", part_01(input.clone()));
    println!("Result part 02: {}", part_02(input.clone()));
}

#[allow(unused_variables)]
fn part_01(input: Vec<&str>) -> i64 {
    const SIM_LENGTH: i64 = 100000;

    let mut moons: Vec<Moon> = Vec::new();
    let re = Regex::new(REGEX_PATTERN).unwrap();

    for row in input {
        let capt = re.captures(row).unwrap();
        
        moons.push(Moon {
            pos: Index3 {
                x: capt.get(1).unwrap().as_str().parse::<i64>().unwrap(),
                y: capt.get(2).unwrap().as_str().parse::<i64>().unwrap(),
                z: capt.get(3).unwrap().as_str().parse::<i64>().unwrap(),
            },
            vel: Index3::zero(),
        });
    }

    for moon in &mut moons {
        moon.tick();
    }

    for i in 0..SIM_LENGTH {
        for moon in 0..moons.len() {
            for target in 0..moons.len() {
                if moon == target {
                    continue;
                }
                let target = moons[target].clone();

                if let Some(moon) = moons.get_mut(moon) {
                    (*moon).apply_gravity(&target);
                }
            }
        }

        for moon in &mut moons {
            moon.tick();
        }
    }

    let mut energy = 0;

    for moon in &mut moons {
        energy += moon.pos.abs().sum() * moon.vel.abs().sum();
    }

    energy
}

#[allow(unused_variables)]
fn part_02(input: Vec<&str>) -> i64 {
    let mut system = System::from(&input);

    let mut x_hashes: HashSet<String> = HashSet::new();
    let mut y_hashes: HashSet<String> = HashSet::new();
    let mut z_hashes: HashSet<String> = HashSet::new();

    let mut x_found = false;
    let mut y_found = false;
    let mut z_found = false;

    let mut x = 0;
    let mut y = 0;
    let mut z = 0;

    while !x_found || !y_found {
        system.tick();

        if !x_found {
            let x_hash = system.get_x_hash();

            if x_hashes.contains(&x_hash) {
                x_found = true;
                x = x_hashes.len();
            } else {
                x_hashes.insert(x_hash);
            }
        }

        if !y_found {
            let y_hash = system.get_y_hash();

            if y_hashes.contains(&y_hash) {
                y_found = true;
                y = y_hashes.len();
            } else {
                y_hashes.insert(y_hash);
            }
        }

        if !z_found {
            let z_hash = system.get_z_hash();

            if z_hashes.contains(&z_hash) {
                z_found = true;
                z = z_hashes.len();
            } else {
                z_hashes.insert(z_hash);
            }
        }
    };

    let t = integer::lcm(x, y);

    let t = integer::lcm(t, z);

    t as i64
}