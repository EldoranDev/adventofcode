use std::ops;
use std::fmt;

#[derive(Clone,Debug, PartialEq, PartialOrd)]
pub struct Index3 {
    pub x: i64,
    pub y: i64,
    pub z: i64,
}

impl Index3 {
    pub fn abs(&self) -> Index3 {
        let mut copy = self.clone();

        copy.x = copy.x.abs();
        copy.y = copy.y.abs();
        copy.z = copy.z.abs();

        copy
    }

    pub fn sum(&self) -> i64 {
        self.x + self.y + self.z
    }

    pub fn zero() -> Index3 {
        Index3 {
            x: 0,
            y: 0,
            z: 0,
        }
    }
}

impl fmt::Display for Index3 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<x={}, y={}, z={}>", self.x, self.y, self.z)
    }
}

impl ops::Add<Index3> for Index3 {
    type Output = Index3;

    fn add(self, rhs: Index3) -> Index3 {
        let mut copy = self.clone();

        copy.x += rhs.x; 
        copy.y += rhs.y;
        copy.z += rhs.z;

        copy
    }
}

impl ops::AddAssign<Index3> for Index3 {
    fn add_assign(&mut self, rhs: Index3) -> ()  {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl ops::Mul<i64> for Index3 {
    type Output = Index3;

    fn mul(self, rhs: i64) -> Index3 {
        let mut copy = self.clone();

        copy.x *= rhs;
        copy.y *= rhs;
        copy.z *= rhs;

        copy
    }
}

