mod utils;
mod intvm;

use std::{thread, time};
use std::fs;
use std::collections::{HashMap, VecDeque};

const INPUT_FILE: &str = "input.txt";
const SHOW_GAME: bool = true;

fn main() {
    let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input = get_input(file);

    println!("Result part 1: {}", part_01(&input));
    println!("Result part 2: {}", part_02(&input));
}

fn get_input(input: &String) -> Vec<&str> {
    let codes = input.split(",");
    codes.collect::<Vec<&str>>()
}

fn part_01(input: &Vec<&str>) -> i64{
    let mut vm = intvm::create_vm(input);

    let mut map: HashMap<(i64, i64), u8> = HashMap::new();
    let mut max_x = 0;
    let mut max_y = 0;

    while !vm.is_halted {
        vm.run();

        let mut y = 0;
        let mut x = 0;

        while vm.output.len() > 0 {
            let point = vm.output.pop_front().unwrap() as u8;
            
            map.insert((x, y), point);

            if x > max_x {
                max_x = x + 1;
            }

            if y > max_y {
                max_y = y;
            }

            match point {
                10 => {
                    y += 1;
                    x = 0;
                },
                _ => {
                    x += 1;
                }
            };
        }

        map.insert((max_x, max_y), 10);

        let intersections = get_intersections(&map, max_x, max_y);
        draw_map(&map, max_x, max_y);
        let mut sum = 0;
        
        for int in intersections {
            sum += int.0 * int.1;
        }

        return sum;
    }

    0
}


fn get_intersections(map: &HashMap<(i64, i64), u8>, x: i64, y: i64) -> Vec<(i64, i64)> {
    let targets = vec![(0, 1), (1, 0), (0, -1), (-1, 0)];
    let mut intersections: Vec<(i64, i64)> = Vec::new();

    for y in 0..y {
        for x in 0..x {
            let p = (x, y);
            let pp = *map.get(&p).or(Some(&0)).unwrap();
        
            if pp == 35 {
                let mut intersection = true;
                
                for target in &targets {
                    let t = *map.get(&(p.0 + target.0, p.1 + target.1)).or(Some(&0)).unwrap();
                
                    if t != 35 {
                        intersection = false;
                        break;
                    }
                }

                if intersection {
                    intersections.push((x, y));
                }
            }

        }
    }

    intersections
}

fn draw_map(map: &HashMap<(i64, i64), u8>, x: i64, y: i64) -> () {
    if !SHOW_GAME {
        return;
    }

    for y in 0..y {
        for x in 0..x {
            print!("{}", *map.get(&(x, y)).or(Some(&64)).unwrap() as char);
        }
    }
}

#[allow(unused_variables)]
fn part_02(input: &Vec<&str>) -> i64{
    let mut vm = intvm::create_vm(input);

    // Wake up robot
    vm.set_mem(0, 2);
    
    vm.run();

    print_output(&mut vm);
    writeln_input(&mut vm, "A,B,A,B,C,A,B,C,A,C");

    vm.run();
    print_output(&mut vm);
    writeln_input(&mut vm, "R,6,L,6,L,10");

    vm.run();
    print_output(&mut vm);
    writeln_input(&mut vm, "L,8,L,6,L,10,L,6");

    vm.run();
    print_output(&mut vm);
    writeln_input(&mut vm, "R,6,L,8,L,10,R,6");

    vm.run();
    print_output(&mut vm);
    writeln_input(&mut vm, "n");

    while !vm.is_halted {
        vm.run();
    }

    vm.output.pop_back().unwrap()
    
}

fn print_output(vm: &mut intvm::IntVM) {
    while vm.output.len() > 0 {
        print!("{}", (vm.output.pop_front().unwrap() as u8) as char);
    }
}

fn writeln_input(vm: &mut intvm::IntVM, input: &str) {
    for c in input.chars() {
        vm.input.push_back(c as i64);
    }

    vm.input.push_back(10);
}

/*
    input.push_str("A,B,A,B,C,A,B,C,A,C");
    input.push('\n');
    input.push_str("R6,L6,L10");
    input.push('\n');
    input.push_str("L8,L6,L10,L6");
    input.push('\n');
    input.push_str("R6,L8,L10,R6");
    input.push('\n');
    input.push('y');
    input.push('\n');
*/