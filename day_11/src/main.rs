mod utils;
mod intvm;
use std::fs;
use std::collections::HashMap;


const INPUT_FILE: &str = "input.txt";
fn main() {
    let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input = get_input(file);

    println!("Result Part 1: {}", part_01(&input));
    part_02(&input);
}

fn get_input(input: &String) -> Vec<&str> {
    let codes = input.split(",");
    codes.collect::<Vec<&str>>()
}

fn part_01(input: &Vec<&str>) -> i64{
    let mut vm = intvm::create_vm(input);
    let mut panels: HashMap<String, i64> = HashMap::new();

    let mut x = 0;
    let mut y = 0;

    let mut direction = 0;

    while (!vm.is_halted) {
    //for _ in 0..5 {
        let mut panel_index = String::new();

        panel_index.push_str(x.to_string().as_ref());
        panel_index.push_str("|");
        panel_index.push_str(y.to_string().as_ref());

        let panel = panels.get(&panel_index).unwrap_or(&0);

        vm.input.push_back(*panel);

        vm.run();

        let out = vm.output.pop_front().unwrap();
        panels.entry(panel_index).and_modify(|e| *e = out).or_insert(out);

        let turn = vm.output.pop_front().unwrap();

        match turn {
            0 => direction -= 1,
            1 => direction += 1,
            _ => panic!("unknown turn direction: {}", turn),
        }

        if direction < 0 {
            direction += 4;
        }

        direction %= 4;
        
        match direction {
            0 => y += 1,
            1 => x += 1,
            2 => y -= 1,
            3 => x -= 1,
            _ => panic!("unknown direction: {}", direction),
        }
    }

    panels.len() as i64
}

fn part_02(input: &Vec<&str>) {
    let mut vm = intvm::create_vm(input);
    let mut panels: HashMap<String, i64> = HashMap::new();

    let mut x = 0;
    let mut y = 0;

    let mut direction = 0;

    let mut panel_index = String::new();

    panel_index.push_str(x.to_string().as_ref());
    panel_index.push_str("|");
    panel_index.push_str(y.to_string().as_ref());

    panels.insert(panel_index, 1);

    while !vm.is_halted {
    //for _ in 0..5 {
        let mut panel_index = String::new();

        panel_index.push_str(x.to_string().as_ref());
        panel_index.push_str("|");
        panel_index.push_str(y.to_string().as_ref());

        let panel = panels.get(&panel_index).unwrap_or(&0);

        vm.input.push_back(*panel);

        vm.run();

        let out = vm.output.pop_front().unwrap();
        panels.entry(panel_index).and_modify(|e| *e = out).or_insert(out);

        let turn = vm.output.pop_front().unwrap();

        match turn {
            0 => direction -= 1,
            1 => direction += 1,
            _ => panic!("unknown turn direction: {}", turn),
        }

        if direction < 0 {
            direction += 4;
        }

        direction %= 4;
        
        match direction {
            0 => y += 1,
            1 => x += 1,
            2 => y -= 1,
            3 => x -= 1,
            _ => panic!("unknown direction: {}", direction),
        }
    }

    for y in (-50..50).rev() {
        for x in -50..50 {
            let mut panel_index = String::new();

            panel_index.push_str(x.to_string().as_ref());
            panel_index.push_str("|");
            panel_index.push_str(y.to_string().as_ref());

            let value = panels.get(&panel_index).or(Some(&0)).unwrap();

            match value {
                0 => print!("."),
                1 => print!("#"),
                _ => print!("."),
            }
        }

        println!();
    }
}