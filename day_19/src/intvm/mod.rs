use crate::utils;
use std::collections::{VecDeque, HashMap};

#[allow(dead_code)]
pub struct IntVM {
    pub rom: Vec<i64>,
    pub input: VecDeque<i64>,
    pub output: VecDeque<i64>,
    pub memory: Vec<i64>,
    pub mem_len: usize,
    pub instr_ptr: usize,
    pub is_halted: bool,
    pub is_waiting_input: bool,
    pub print_output: bool,
    pub relative_offset: i64,

    pub extended_memory: HashMap<usize, i64>,

    update_pointer: bool,
    tick_length: usize,
}

impl IntVM {
    #[allow(dead_code)]
    pub fn reset(&mut self) {
        self.memory = self.rom.clone();
        self.extended_memory = HashMap::new();
        self.instr_ptr = 0;
        self.is_halted = false;
    }

    #[allow(dead_code)]
    pub fn set_mem(&mut self, address: usize, value: i64) {
        if self.mem_len > address {
            self.memory[address] = value;
        } else {
            *self.extended_memory.entry(address).or_insert(0) =  value;
        }
    }

    pub fn get_mem(&self, address: usize) -> i64 {
        if self.mem_len > address {
            return self.memory[address];
        }

        **self.extended_memory.get(&address).get_or_insert(&0)
    }

    pub fn get_input(&mut self, arg_count: usize) -> i64 {
        let instruction = self.get_mem(self.instr_ptr);
        let mode = (instruction / (10 as i64).pow((arg_count + 2) as u32)) % 10;

        if arg_count + 2 > self.tick_length {
            self.tick_length = arg_count + 2;
        }

        match mode {
            0 => self.get_mem(self.get_mem(self.instr_ptr + 1 + arg_count) as usize),
            1 => self.get_mem(self.instr_ptr + 1 + arg_count),
            2 => self.get_mem((self.relative_offset + self.get_mem(self.instr_ptr+1+arg_count)) as usize),
            _ => self.get_mem(self.get_mem(self.instr_ptr + 1 + arg_count) as usize),
        }
    }

    pub fn get_target(&mut self, arg_count: usize) -> usize {
        if arg_count + 2 > self.tick_length {
            self.tick_length = arg_count + 2;
        }

        let instruction = self.get_mem(self.instr_ptr);
        let mode = (instruction / (10 as i64).pow((arg_count + 2) as u32)) % 10;

        if arg_count + 2 > self.tick_length {
            self.tick_length = arg_count + 2;
        }

        match mode {
            2 => (self.relative_offset + self.get_mem(self.instr_ptr+1+arg_count)) as usize,
            _ => self.get_mem(self.instr_ptr + arg_count + 1) as usize,
        }
    }

    pub fn jump(&mut self, addr: usize) {
        self.instr_ptr = addr;
        self.update_pointer = false;
    }

    fn pre_tick(&mut self) {
        self.update_pointer = true;
        self.tick_length = 0;
    }

    fn post_tick(&mut self) {
        if self.update_pointer {
            self.instr_ptr = self.instr_ptr + self.tick_length;
        }
    }

    #[allow(dead_code)]
    pub fn tick(&mut self) {
        if self.is_halted || self.is_waiting_input {
            return;
        }

        self.pre_tick();
        
        let op: i64 = self.get_mem(self.instr_ptr) % 100;

        match op {
            1 => {                
                let a = self.get_input(0);
                let b = self.get_input(1);
                
                let target = self.get_target(2);
                self.set_mem(target, a + b);
            },
            2 => {
                let a = self.get_input(0);
                let b = self.get_input(1);

                let target = self.get_target(2);
                self.set_mem(target, a * b);
            },
            3 => {
                let target = self.get_target(0);
                
                if let Some(input) = self.input.pop_front() {
                    self.set_mem(target, input);
                } else {
                    self.is_waiting_input = true;
                    return;
                }         
            },
            4 => {
                let target = self.get_input(0);
                
                self.output.push_back(target as i64);
                                
                if self.print_output {
                    println!("OUT: {}", target);
                }
            },
            5 => {
                let a = self.get_input(0);
                let b = self.get_input(1);

                if a != 0 {
                    self.jump(b as usize);                    
                }
            },
            6 => {
                let a = self.get_input(0);
                let b = self.get_input(1);

                if a == 0 {
                    self.jump(b as usize);
                }

            },
            7 => {
                let a = self.get_input(0);
                let b = self.get_input(1);

                let target = self.get_target(2);
                
                if a < b {
                    self.set_mem(target, 1);
                } else {
                    self.set_mem(target, 0);
                }
            },
            8 => {
                let a = self.get_input(0);
                let b = self.get_input(1);                

                let target = self.get_target(2);

                if a == b {
                    self.set_mem(target, 1);
                } else {
                    self.set_mem(target, 0);
                }
            },
            9 => {
                let a = self.get_input(0);
                self.relative_offset += a;
            },
            99 => {
                self.is_halted = true;
            },
            _ => {
                println!("Unknown OPCode ({}, {}, {}, {}) update IntVM", op, self.get_mem(self.instr_ptr + 1), self.get_mem(self.instr_ptr + 2), self.get_mem(self.instr_ptr + 3));
                self.is_halted = true;
            },
        }

        self.post_tick();
    }

    #[allow(dead_code)]
    pub fn get_output(&mut self) -> i64 {
        self.get_mem(0)
    }

    #[allow(dead_code)]
    pub fn run(&mut self) -> i64 {
        self.is_waiting_input = false;
        
        while !self.is_halted && !self.is_waiting_input {
            self.tick()
        };

        self.get_output()
    }
}

#[allow(dead_code)]
pub fn create_vm(input: &Vec<&str>) -> IntVM {
    let rom = utils::vec_to_int(input.clone());
    let mut vm = IntVM {        
        rom: rom.clone(),
        print_output: false,
        instr_ptr: 0,
        is_halted: false,
        is_waiting_input: false,
        memory: rom.clone(),
        mem_len: rom.len(),
        extended_memory: HashMap::new(),
        input: VecDeque::new(),
        output: VecDeque::new(),
        relative_offset: 0,
        update_pointer: true,
        tick_length: 0,
    };

    vm
}