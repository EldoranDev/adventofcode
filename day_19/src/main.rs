mod utils;
mod intvm;

use std::fs;
use std::collections::VecDeque;

const INPUT_FILE: &str = "input.txt";

fn main() {
    let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input = get_input(file);

    println!("Result part 1: {}", part_01(&input));
    println!("Result part 2: {}", part_02(&input));
}

fn get_input(input: &String) -> Vec<&str> {
    let codes = input.split(",");
    codes.collect::<Vec<&str>>()
}

fn part_01(input: &Vec<&str>) -> i64 {
    let mut vm = intvm::create_vm(input);
    let mut count = 0;
    for y in 0..50 {
        let mut row = 0;
        print!("{}|", y);
        for x in 0..50 {
            vm.input.push_back(x);
            vm.input.push_back(y);

            vm.run();

            let output = vm.output.pop_back().unwrap();
            print!("{}", output);
            if output == 1 {
                count += 1;
                row += 1;
            }

            vm.reset();
        }

        println!("|{}", row);
    }
    

    count
}


#[allow(unused_variables)]
fn part_02(input: &Vec<&str>) -> i64{
    let mut vm = intvm::create_vm(input);

    let offset = (3, 5);
    let found = false;

    let mut current = (0, 100);

    for _ in 0..10000 {
        while !is_on_beam(&mut vm, current) {
            current.0 += 1;
        }

        for offset in 0..200 {
            if !is_on_beam(&mut vm, (current.0 + offset + 99, current.1)) {
                break;
            }

            if !is_on_beam(&mut vm, (current.0 + offset, current.1 + 99)) {
                continue;
            }

            return (current.0 + offset) * 10_000 + current.1;
        }

        current.1 += 1;
    }

    panic!("Did not find a solution");
}

fn is_on_beam(vm: &mut intvm::IntVM, coords: (i64, i64)) -> bool {
    vm.reset();

    vm.input.push_back(coords.0);
    vm.input.push_back(coords.1);

    vm.run();

    if let Some(output) = vm.output.pop_back() {
        return output == 1;
    }

    panic!("VM Crashed");
}
