use std::fs;

const INPUT_FILE: &str = "input.txt";

fn main() {
    let input = fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input = input.lines();
    let input: Vec<&str> = input.collect::<Vec<&str>>();

    part_01(input.clone());
    part_02(input.clone());
}

#[allow(unused_variables)]
fn part_01(input: Vec<&str>) {
    println!("Result of Part 1");
}

#[allow(unused_variables)]
fn part_02(input: Vec<&str>) {
    println!("Result of Part 2");
}