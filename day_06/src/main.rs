mod utils;
mod intvm;
use std::fs;
use std::collections::HashMap;

const INPUT_FILE: &str = "input.txt";

fn main() {
    let input = fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input = input.lines();
    let input: Vec<&str> = input.collect::<Vec<&str>>();

    part_01(input.clone());
    part_02(input.clone());
}

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct Element {
    pub parent: String,
    pub children: Vec<String>,
}

#[allow(unused_variables)]
fn part_01(input: Vec<&str>) {
    let mut map: HashMap<String, Element> = HashMap::new();

    for line in input {
        let parts = line.split(")");
        let parts = parts.collect::<Vec<&str>>();

        if !map.contains_key(parts[0]) {
            map.insert(parts[0].to_string(), Element {
                parent: String::from(""),
                children: Vec::new(),
            });    
        }

        let parent: &Element = map.get(parts[0]).expect("We checked, but parent did not exist");
        //let parent = &parent;

        if !map.contains_key(parts[1]) { 
            map.insert(parts[1].to_string(), Element {
                parent: String::from(""),
                children: Vec::new(),
            });   
        }

        if let Some(child) = map.get_mut(parts[1]) {
            child.parent = parts[0].to_string();
        }
    }

    println!("Number of Planets: {}", map.len());

    let mut count = 0;

    for planet in &map {
        count = count + get_orbit_count(&planet.1, &map)
    }

    println!("Number of orbits: {}", count);
}

fn get_orbit_count(element: &Element, map: &HashMap<String, Element>) -> i64 {
    let mut count = 0;
    let mut current = &element.parent;

    while current != "" {
        count = count + 1;
        let next = map.get(current).expect("Error input data");
        current = &next.parent;
    }

    count
}

#[allow(unused_variables)]
fn part_02(input: Vec<&str>) {
    let mut map: HashMap<String, Element> = HashMap::new();

    for line in input {
        let parts = line.split(")");
        let parts = parts.collect::<Vec<&str>>();

        if !map.contains_key(parts[0]) {
            map.insert(parts[0].to_string(), Element {
                parent: String::from(""),
                children: Vec::new(),
            });    
        }

        if !map.contains_key(parts[1]) { 
            map.insert(parts[1].to_string(), Element {
                parent: String::from(""),
                children: Vec::new(),
            });   
        }

        if let Some(child) = map.get_mut(parts[1]) {
            child.parent = parts[0].to_string();
        }

        if let Some(parent) = map.get_mut(parts[0]) {
            parent.children.push(parts[0].to_string());
        }
    }

    let meeting_point = search_path(&map);
    
    let you = count_steps("YOU", meeting_point, &map);
    let san = count_steps("SAN", meeting_point, &map);

    println!("Result Part 2: {}", you + san - 2);
}

// Thanks to @hyperkubus for explaining to me that im thinking way to complicated
fn search_path(map: &HashMap<String, Element>) -> &str {
    let mut current_a: &str = "YOU";
    let mut current_b: &str = "SAN";

    let mut v_a: Vec<&str> = Vec::new();
    let mut v_b: Vec<&str> = Vec::new();

    let mut meeting: &str = "";

    while meeting == "" {
        let a: &Element = map.get(current_a).expect("Data inconsitency");

        v_a.push(&a.parent);
        current_a = a.parent.as_ref();

        if v_b.contains(&current_a) {
            meeting = current_a;
        }

        if current_b != "" {
            let b: &Element = map.get(current_b).expect("Data inconsitency");

            v_b.push(&b.parent);
            current_b = &b.parent;

            if v_a.contains(&current_b) {
                meeting = current_b;
            }
        }
    }

    meeting
}

fn count_steps(from: &str, to: &str, map: &HashMap<String, Element>) -> i64 {
    let mut count = 0;
    let mut current = from;

    while current != to {
        count = count + 1;
        let next = map.get(current).expect("Error input data");
        current = &next.parent;
    }

    count
} 