mod utils;
mod intvm;
use std::fs;

const INPUT_FILE: &str = "input.txt";

fn main() {
    let input = fs::read_to_string(INPUT_FILE).expect("Error reading input");

    let input = input.lines();
    let input: Vec<&str> = input.collect::<Vec<&str>>();

    println!("Result part 1: {}", part_01(input.clone()));
    println!("Result part 2: {}", part_02(input.clone()));
}

#[derive(Debug, PartialEq, PartialOrd, Clone)]
struct Asteroid(f64, f64);

#[allow(unused_variables)]
fn part_01(input: Vec<&str>) -> i64 {
    
    let mut y = 0.0;
    let mut asteroids: Vec<Asteroid> = Vec::new();
    for yy in input {
        let mut x = 0.0;

        for xx in yy.chars() {
            if xx == '#' {
                asteroids.push(Asteroid(x, y));
            }
            x += 1.0;
        }

        y += 1.0;
    }

    let mut max = 0;

    for asteroid in &asteroids {
        let count = get_count_in_los(&asteroid, &asteroids);

        if count > max {
            max = count;
        }
    }

    max
}

fn get_count_in_los(a: &Asteroid, list: &Vec<Asteroid>) -> i64 {
    let mut something_between;
    let mut count = 0;

    for b in list {
        something_between = false;

        for c in list {
            if a == c || b == c {
                continue;
            }

            let cross: f64 = ((c.1 - a.1) * (b.0 - a.0)) - ((c.0 - a.0) * (b.1 - a.1));

            if cross.abs() > core::f64::EPSILON {
                continue;
            }

            let dot: f64 = (c.0 - a.0) * (b.0 - a.0) + ((c.1 - a.1) * (b.1 - a.1));
            
            if dot < 0.0 {
                continue;
            }

            let square = (b.0 - a.0)*(b.0 - a.0) + (b.1 - a.1)*(b.1 - a.1);

            if dot > square {
                continue;
            }

            something_between = true;
            break;
        }

        if !something_between {
            count += 1;
        }
    }

    count
}

fn get_asteroids_in_los(a: &Asteroid, list: &Vec<Asteroid>) -> Vec<Asteroid> {
    let mut asteroids: Vec<Asteroid> = Vec::new();

    let mut something_between;

    for b in list {
        something_between = false;

        for c in list {
            if a == c || b == c {
                continue;
            }

            let cross: f64 = ((c.1 - a.1) * (b.0 - a.0)) - ((c.0 - a.0) * (b.1 - a.1));

            if cross.abs() > core::f64::EPSILON {
                continue;
            }

            let dot: f64 = (c.0 - a.0) * (b.0 - a.0) + ((c.1 - a.1) * (b.1 - a.1));
            
            if dot < 0.0 {
                continue;
            }

            let square = (b.0 - a.0)*(b.0 - a.0) + (b.1 - a.1)*(b.1 - a.1);

            if dot > square {
                continue;
            }

            something_between = true;
            break;
        }

        if !something_between {
            asteroids.push(b.clone());
        }
    }

    asteroids
}

fn part_02(input: Vec<&str>) -> i64 {
    let mut y = 0.0;
    let mut asteroids: Vec<Asteroid> = Vec::new();
    for yy in input {
        let mut x = 0.0;

        for xx in yy.chars() {
            if xx == '#' {
                asteroids.push(Asteroid(x, y));
            }

            x += 1.0;
        }

        y += 1.0;
    }

    let base = Asteroid(28.0, 29.0);

    let mut list = get_asteroids_in_los(&base, &asteroids);

    list.sort_by(|e, b| ((b.0 - base.0).atan2(b.1 - base.1)).partial_cmp(&((e.0 - base.0).atan2(e.1 - base.1))).unwrap());

    (list[199].0 as i64) * 100 + (list[199].1 as i64)
}