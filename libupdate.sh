#!/bin/env bash
# Update lib based on day

while getopts d: opt
do
    case $opt in
        d) DAY=$OPTARG;;
    esac
done

DAY_PADDED=`printf %02d $DAY`
SOURCE_FOLDER="day_$DAY_PADDED"

VM_DAYS=("02" "05")

if [ "$DAY" = "" ]; then
    echo -n -e "\e[31m\e[1m"
    echo "Day needs to be provided"
    echo -n -e "\e[0m"
    exit
fi

echo "Updating modules using Day $DAY as source"

echo "Found helper modules: "
modules=$(find "$SOURCE_FOLDER/src"  -mindepth 1 -maxdepth 1 -type d -printf '%f\n')

echo $modules

targets=$(find . -mindepth 1 -maxdepth 1 -name "day_*" -type d -not -path "./$SOURCE_FOLDER")

for target in $targets
do
    for module in $modules
    do
        rsync -a --delete "$SOURCE_FOLDER/src/$module" "$target/src/"
    done
done

rsync -a --delete "$SOURCE_FOLDER/src/$module" "__template/src/"

dir=`pwd`

for day in "${VM_DAYS[@]}"
do
    cd "day_$day"
    cargo test
    cd dir
done
