mod utils;
mod intvm;

use std::{thread, time};
use std::fs;
use std::collections::{HashMap, VecDeque};

const INPUT_FILE: &str = "input.txt";
const SHOW_GAME: bool = true;

fn main() {
    let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input = get_input(file);

    println!("Result part 1: {}", part_01(&input));
    println!("Result part 2: {}", part_02(&input));
}

fn get_input(input: &String) -> Vec<&str> {
    let codes = input.split(",");
    codes.collect::<Vec<&str>>()
}

fn part_01(input: &Vec<&str>) -> i64{
    let vm = intvm::create_vm(input);

    let mut map: HashMap<(i64, i64), i64> = HashMap::new();
    let mut next_queue: VecDeque<((i64, i64), intvm::IntVM, i64)> = VecDeque::new();

    let mut min = (0, 0);
    let mut max = (0, 0);
    
    next_queue.push_back(((0,0), vm.clone(), 0));
    map.insert((0, 0), 3);
    let mut distance = 0;

    while next_queue.len() > 0 {
        let next = next_queue.pop_front().unwrap();
        let pos: (i64, i64) = next.0;

        let targets = vec![ 
            ((pos.0 + 1, pos.1), 4),
            ((pos.0 - 1, pos.1), 3),
            ((pos.0, pos.1 + 1), 2),
            ((pos.0, pos.1 - 1), 1),
        ];
        
        for target in targets {
            if !map.contains_key(&target.0) {
                let mut vm: intvm::IntVM = next.1.clone();
                let found = walk(&mut vm, target.1);
                let pos = target.0;

                map.insert(pos, found);
                
                if found == 1 || found == 2 {
                    next_queue.push_back((pos, vm, next.2 + 1));

                    if found == 2 {
                        distance = next.2 + 1;
                    }
                }

                if pos.0 < min.0 {
                    min.0 = pos.0;
                }

                if pos.0 > max.0 {
                    max.0 = pos.0;
                }

                if pos.1 < min.1 {
                    min.1 = pos.1;
                }

                if pos.1 > max.1 {
                    max.1 = pos.1;
                }
            }
        }

        draw_map(&min, &max, &map, '?');
    }

    draw_map(&min, &max, &map, '▒');

    distance
}

fn walk(vm: &mut intvm::IntVM, direction: i64) -> i64 {
    vm.input.push_back(direction);
    vm.run();

    vm.output.pop_front().unwrap()
}


fn draw_map(min: &(i64, i64), max: &(i64, i64), map: &HashMap<(i64, i64), i64>, unknown: char) -> () {
    if !SHOW_GAME {
        return ();
    }

    let mut output = String::new();

    for y in (min.1-1)..(max.1+2) {
        for x in (min.0-1)..(max.0+2) {
            match map.get(&(x, y)).or(Some(&-1)).unwrap() {
                0 => output.push('▒'),
                1 => output.push(' '),
                2 => output.push('X'),
                3 => output.push('O'),
                _ => output.push(unknown),
            };
        }
        output.push('\n');
    }

    print!("{}[2J", 27 as char);
    print!("{}", output);
    let time = time::Duration::from_millis(5);
    thread::sleep(time);
}

fn part_02(input: &Vec<&str>) -> i64 {
    let vm = intvm::create_vm(input);

    let mut map: HashMap<(i64, i64), i64> = HashMap::new();
    let mut next_queue: VecDeque<((i64, i64), intvm::IntVM, i64)> = VecDeque::new();

    let mut min = (0, 0);
    let mut max = (0, 0);
    
    next_queue.push_back(((0,0), vm.clone(), 0));
    map.insert((0, 0), 3);

    let mut start = (0, 0);

    while next_queue.len() > 0 {
        let next = next_queue.pop_front().unwrap();
        let pos: (i64, i64) = next.0;

        let targets = vec![ 
            ((pos.0 + 1, pos.1), 4),
            ((pos.0 - 1, pos.1), 3),
            ((pos.0, pos.1 + 1), 2),
            ((pos.0, pos.1 - 1), 1),
        ];
        
        for target in targets {
            if !map.contains_key(&target.0) {
                let mut vm: intvm::IntVM = next.1.clone();
                let found = walk(&mut vm, target.1);
                let pos = target.0;

                map.insert(pos, found);
                
                if found == 1 || found == 2 {
                    next_queue.push_back((pos, vm, next.2 + 1));

                    if found == 2 {
                        start = pos;
                    }
                }

                if pos.0 < min.0 {
                    min.0 = pos.0;
                }

                if pos.0 > max.0 {
                    max.0 = pos.0;
                }

                if pos.1 < min.1 {
                    min.1 = pos.1;
                }

                if pos.1 > max.1 {
                    max.1 = pos.1;
                }
            }
        }
    }

    let mut next_queue: VecDeque<((i64, i64), i64)> = VecDeque::new();

    next_queue.push_back((start, 0));
    let mut highest = 0;

    while next_queue.len() > 0 {
        let current = next_queue.pop_front().unwrap();
        let pos = current.0;

        if current.1 > highest {
            highest = current.1;
        }

        let targets = vec![ 
            (pos.0 + 1, pos.1),
            (pos.0 - 1, pos.1),
            (pos.0, pos.1 + 1),
            (pos.0, pos.1 - 1),
        ];
        
        for target in targets {
            let tile = map.get(&target).or(Some(&1)).unwrap();

            if tile == &1 {
                next_queue.push_back((target,  current.1 + 1));
                map.entry(target).and_modify(|e| *e = 3);
            }
            
        }
        
        draw_map(&min, &max, &map, '▒');
    }

    highest
}