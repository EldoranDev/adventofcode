#!/bin/env bash
# Create AdventOfCode Day

function join_nl { local IFS=$'\n'; echo "$*"; }

while getopts rd: opt
do
    case $opt in
        d) DAY=$OPTARG;;
        r) RECREATE=1;;
    esac
done

AOC_YEAR=2019
AOC_URL_BASE="https://adventofcode.com/$AOC_YEAR"

DAY_PADDED=`printf %02d $DAY`
TARGET_FOLDER="day_$DAY_PADDED"

if [ "$RECREATE" = "1" ]; then
    echo -n -e "\e[31m\e[1m"
    echo "RECREATING DAY (this will override the day if it exists)"
    echo -n -e "\e[0m"

    sleep "0s"

    rm -Rf $TARGET_FOLDER
fi

if [ "$DAY" = "" ]; then
    echo -n -e "\e[31m\e[1m"
    echo "Day needs to be provided"
    echo -n -e "\e[0m"
    exit
fi

if [ -d "$TARGET_FOLDER" ]; then
    echo -n -e "\e[31m\e[1m"
    echo "Day already exists, use -r to restart"
    echo -n -e "\e[0m"
    exit
fi

AOC_URL="$AOC_URL_BASE/day/$DAY/input"
COOKIE_SESSION=`cat .token`;

echo "Creating day ${DAY_PADDED}"

# Copy template and replace revenreces
cp -R "__template" "$TARGET_FOLDER"
find "./$TARGET_FOLDER" -type f -not -path "./$TARGET_FOLDER/target/*" -exec sed -i -e "s/day_xx/$TARGET_FOLDER/g" {} \;

curl $AOC_URL \
    -s \
    --cookie "session=$COOKIE_SESSION" \
    > "$TARGET_FOLDER/input.txt"

modules=$(find "$TARGET_FOLDER/src"  -mindepth 1 -maxdepth 1 -type d -printf '%f\n')

readarray -t modules <<<"$modules"

mods=()
for mod in "${modules[@]}"
do
    mods+=("mod $mod;")
done

modules_header=$(join_nl "${mods[@]}")

echo "${modules_header}" | cat - "$TARGET_FOLDER/src/main.rs" > tmp && mv tmp $TARGET_FOLDER/src/main.rs

git add $TARGET_FOLDER
git commit -m "create($TARGET_FOLDER): Initial creation of day $DAY_PADDED"

cd "$TARGET_FOLDER"

code .