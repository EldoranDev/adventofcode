mod utils;
mod intvm;
use std::fs;
use std::collections::HashMap;

const INPUT_FILE: &str = "input.txt";

fn main() {
    let input = fs::read_to_string(INPUT_FILE).expect("Error reading input");

    println!("Result of Part 1: {}", part_01(&input));
    part_02(&input);
}

struct Layer {
    chars: HashMap<char, i64>,
    pixel: Vec<char>,
}

#[allow(unused_variables)]
fn part_01(input: &String) -> i64 {
    const WIDTH: usize = 25;
    const HEIGHT: usize = 6;
    let layer_length: usize = (WIDTH * HEIGHT) as usize;
    let layer_count = input.len()/layer_length;

    let mut layers: Vec<Layer> = Vec::new();

    let mut lowest = 10000000000000;
    let mut lowest_index: i64 = -1;

    for l in 0..layer_count {
        let mut layer = Layer {
            chars: HashMap::new(),
            pixel: Vec::new(),
        };

        let layer_content = &input[(l*layer_length)..l*layer_length+layer_length];
        
        for p in layer_content.chars() {
            layer.pixel.push(p);

            layer.chars.entry(p)
                .and_modify(|e| { *e += 1 })
                .or_insert(1);
        }

        let count = *layer.chars.get(&'0').unwrap();

        if lowest > count {
            lowest_index = l as i64;
            lowest = count;
        }

        layers.push(layer);
    }

    let ones = *layers[lowest_index as usize].chars.entry('1').or_default();
    let twos = *layers[lowest_index as usize].chars.entry('2').or_default();

    ones * twos
}

#[allow(unused_variables)]
fn part_02(input: &String) {
    const WIDTH: usize = 25;
    const HEIGHT: usize = 6;

    let layer_length: usize = (WIDTH * HEIGHT) as usize;
    let layer_count = input.len()/layer_length;

    let mut image: [char; WIDTH*HEIGHT] = ['2'; 150];

    for l in 0..layer_count {
        let layer_content = &input[(l*layer_length)..l*layer_length+layer_length];
        
        let mut index: usize = 0;

        for p in layer_content.chars() {

            if image[index] == '2' {
                image[index] = p;
            }

            index += 1;
        }
    }
    
    println!("Result of Part 2: ");

    for y in 0..HEIGHT {
        let mut row = String::new();

        for x in 0..WIDTH {
            row.push(match image[y * WIDTH + x] {
                '0' => '█',
                '1' => ' ',
                _ => ' ',
            });
        }
        println!("{}", row);
    }
}