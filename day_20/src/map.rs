use std::ops::{Add, AddAssign, Sub, SubAssign};
use std::collections::HashMap;

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Pos {
    pub x: i32,
    pub y: i32,
}

impl Pos {
    pub fn distance(&self, other: &Self) -> u32 {
        ((self.x - other.x).abs() + (self.y - other.y).abs()) as u32
    }

    pub fn successors(&self, network: &HashMap<Pos, Pos>) -> Vec<(Pos, u32)> {
        let &Pos{x, y} = self;
        let mut succs = vec![Pos{x: x+1, y}, Pos{x: x-1,y}, Pos{x,y: y+1}, Pos{x,y: y-1}];

        if let Some(t) = network.get(self) {
            succs.push(t.clone());
        }

        succs.into_iter().map(|p| (p, 1)).collect()
      }
}

impl Add for Pos {
    type Output = Pos;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl AddAssign for Pos {
    fn add_assign(&mut self, other: Self) {
        *self = Self {
            x: self.x + other.x,
            y: self.y + other.y,
        };
    }
}

impl Sub for Pos {
    type Output = Pos;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl SubAssign for Pos {
    fn sub_assign(&mut self, other: Self) {
        *self = Self {
            x: self.x - other.x,
            y: self.y - other.y,
        };
    }
}
