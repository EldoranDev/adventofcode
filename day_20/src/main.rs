mod utils;
mod intvm;
mod map;

use std::fs;
use std::collections::HashMap;
use map::Pos;
use pathfinding::directed::astar;
use pathfinding::directed::dijkstra;

const INPUT_FILE: &str = "input.txt";

type Map = HashMap<Pos, char>;
type JumpTable = HashMap<Pos, Pos>;

fn main() {
    let input = fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input = input.lines();
    let input: Vec<&str> = input.collect::<Vec<&str>>();

    part_01(input.clone());
    part_02(input.clone());
}

#[allow(unused_variables)]
fn part_01(input: Vec<&str>) {
    let (map, dimensions) = build_map(input);
    let ( start, goal, network ) = analyze_network(&map, &dimensions);
    println!("Map dimensions: {:?}", dimensions);
    println!("Found start at: {:?}", start);
    println!("Found goal at: {:?}", goal);
    println!("Found {} jumps", network.len());
    //println!("Found {:?} jumps", network);

    let pp = Pos{x: 2, y: 8};
    let successors = |p: &Pos| {
        p.successors(&network)
            .iter()
            .filter(|f| {
                let pp = map.get(&f.0);
                match pp {
                    Some(x) => {
                        match x {
                            '.' => true,
                            _ => false,
                        }
                    },
                    None => {
                        false
                    }
                }
            })
            .map(|pp| pp.clone())
            .collect::<Vec<(Pos, u32)>>()
    };

    //print_map(dimensions, &map, &network);

    let result = dijkstra::dijkstra(
        &start,
        successors, 
        //|p| p.distance(&goal),
        |p| *p == goal
    );

    println!("Path: {:?}",result);
}

fn build_map(input: Vec<&str>) -> (Map, (i32, i32)) {
    let mut map: Map = Map::new();

    let mut y = 0;
    let mut x = 0;
    
    let mut dimensions: (i32, i32) = (
        0,
        input.len() as i32,
    );

    // Save map to our dict
    for line in input {
        for c in line.chars() {
            if c != ' ' {
                map.insert(Pos{x, y}, c);
            }

            x += 1;

            if x > dimensions.0 {
                dimensions.0 = x - 1;
            }
        }

        y += 1;
        x = 0;
    }

    (map, (130, 130))
}

fn analyze_network(map: &Map, dimensions: &(i32, i32)) -> (Pos, Pos, JumpTable) {
    let mut table: JumpTable = JumpTable::new();
    let mut start = Pos{x: 0, y: 0};
    let mut goal = Pos{x: 0, y: 0};

    let mut cache: HashMap<String, Pos> = HashMap::new();

    let neighbours = vec![
        Pos{x:  0, y: -1},
        Pos{x:  0, y:  1},
        Pos{x: -1, y:  0},
        Pos{x:  1, y:  0},
    ];

    'column: for x in 0..dimensions.0 {
        'row: for y in 0..dimensions.1 {
            let p = Pos{x, y};

            if let Some(c) = map.get(&p) {
                match c {
                    'A' ..= 'Z' => (),
                    _ => continue 'row,
                }

                for neighbour in &neighbours {
                    
                    let t = p.clone() + neighbour.clone();
                    let tt = p.clone() + neighbour.clone() + neighbour.clone();
                    if let Some(cc) = map.get(&t) {
                        let ccc = map.get(&tt).or(Some(&'#')).unwrap();

                        if *ccc != '.'{
                            continue;
                        } 

                        if cc == c {
                            // we need that check here so we can use the right portal stuff {
                            if *c == 'A' {
                                start = tt.clone();
                                continue 'row;
                            }
                
                            if *c == 'Z' {
                                goal = tt.clone();
                                continue 'row;
                            }
                        } else if *cc >= 'A' && *cc <= 'Z' {
                            let mut key = String::new();
                            // We have an error here, labels should always be red von top -> bottom, left -> right
                            key.push(*c);
                            key.push(*cc);
                            

                            if let Some(ca) = cache.get(&key) {
                                
                                table.insert(ca.clone(), tt.clone());
                                table.insert(tt.clone(), ca.clone());

                                continue 'row;
                            } else {
                                println!("Inserting into cache: {} (new len: {}", key, cache.len());
                                cache.insert(key, tt);
                                
                                continue 'row;
                            }
                        }
                    }
                }
            }
        }
    }

    //println!("{:#?}", cache);

    (start, goal, table)
}

fn print_map(dimensions: (i32, i32), map: &Map, network: &JumpTable) {
    for y in 0..dimensions.1 {
        for x in 0..dimensions.0 {
            if let Some(c) = map.get(&Pos{x, y}) {
                if let Some(j) = network.get(&Pos{x, y}) {
                    print!("!");
                } else {
                    print!("{}", c);
                }
                
            } else {
                print!(" ");
            }
        }
        println!();
    }
}

#[allow(unused_variables)]
fn part_02(input: Vec<&str>) {
    println!("Result of Part 2");
}
