mod utils;
mod intvm;
use std::{thread, time};
use std::fs;
use std::collections::HashMap;


const INPUT_FILE: &str = "input.txt";
const SHOW_GAME: bool = false;

fn main() {
    let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input = get_input(file);

    println!("Result Part 1: {}", part_01(&input));
    part_02(&input);
}

fn get_input(input: &String) -> Vec<&str> {
    let codes = input.split(",");
    codes.collect::<Vec<&str>>()
}

fn part_01(input: &Vec<&str>) -> i64{
    let mut vm = intvm::create_vm(input);

    vm.run();

    let mut boxes = 0;

    let len =  vm.output.len();
    
    while vm.output.len() > 0 {

        let x = vm.output.pop_front().unwrap();
        let y = vm.output.pop_front().unwrap();
        let id = vm.output.pop_front().unwrap();

        if id == 2 {
            boxes += 1;
        }  
    };

    boxes
}

fn part_02(input: &Vec<&str>) {
    let mut vm = intvm::create_vm(input);

    vm.set_mem(0, 2);

    let mut max_x = 0;
    let mut max_y = 0;

    let mut screen: HashMap<String, i64> = HashMap::new();
    let mut score = 0;

    let mut ball = 0;
    let mut paddle = 0;

    while !vm.is_halted {
        vm.run();

        let mut output = String::new();

        while vm.output.len() > 0 {
            let x = vm.output.pop_front().unwrap();
            let y = vm.output.pop_front().unwrap();
            
            let val = vm.output.pop_front().unwrap();

            if x == -1 && y == 0 {
                score = val;
            } else {
                let mut key = String::new();
                key.push_str(&x.to_string());
                key.push('-');
                key.push_str(&y.to_string());

                screen.entry(key).and_modify(|e| *e = val).or_insert(val);

                if val == 3 {
                    paddle = x;
                }

                if val == 4 {
                    ball = x;
                }

                if x > max_x {
                    max_x = x+1;
                }

                if y > max_y {
                    max_y = y;
                }
            }

            if SHOW_GAME {
                for iy in 0..max_y {
                    for ix in 0..max_x {
                        let mut key = String::new();

                        key.push_str(&ix.to_string());
                        key.push('-');
                        key.push_str(&iy.to_string());

                        match screen.get(&key).unwrap() {
                            0 => output.push(' '),
                            1 => output.push('|'),
                            2 => output.push('#'),
                            3 => {
                                output.push('_')
                            },
                            4 => {
                                output.push('x');
                            },
                            _ => output.push('?'),
                        }
                    }

                    output.push('\n');
                } 
            }
        }


        if ball > paddle {
            vm.input.push_back(1);
        } else if ball < paddle {
            vm.input.push_back(-1);
        } else {
            vm.input.push_back(0);
        }

        if SHOW_GAME {
            print!("{}[2J", 27 as char);
            print!("{}", output);
            println!("Player Score: {}", score);
            thread::sleep_ms(1);
        }
    }

    println!("Player Score: {}", score);
}