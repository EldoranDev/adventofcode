mod utils;
mod intvm;
use std::fs;
use std::time::Instant;

const INPUT_FILE: &str = "input.txt";
fn main() {
    let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input = get_input(file);

    println!("Result Part 1: {}", part_01(&input));
    println!("Result Part 2: {}", part_02(&input));
}

fn get_input(input: &String) -> Vec<&str> {
    let codes = input.split(",");
    codes.collect::<Vec<&str>>()
}

fn part_01(input: &Vec<&str>) -> i64{
    let mut vm = intvm::create_vm(input);

    vm.input.push_back(1);
    vm.run();

    vm.output.pop_back().expect("")
}

fn part_02(input: &Vec<&str>) -> i64{
    let mut vm = intvm::create_vm(input);

    vm.input.push_back(2);
    vm.run();

    vm.output.pop_back().expect("")
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part01()  {
        let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
        let input = get_input(file);
    
        assert_eq!(part_01(&input), 3340912345);
    }

    #[test]
    fn test_part02()  {
        let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
        let input = get_input(file);
    
        assert_eq!(part_02(&input), 51754);
    }

    #[test]
    fn test_part01_1st() {
        let rom: Vec<i64> = vec![109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99];
        let rom: Vec<String> = rom.into_iter().map(|x| x.to_string()).collect();
        let rom: String = rom.join(",");
        let file = &String::from(&rom);
        let input = get_input(file);
    

        let mut vm = intvm::create_vm(&input);
        vm.run();

        let output: String = vm.output.into_iter().map(|x| x.to_string()).collect::<Vec<String>>().join(",");
        
        assert_eq!(
            rom,
            output
        );
    }

    #[test]
    fn test_part01_2nd() {
        let rom: Vec<i64> = vec![1102,34915192,34915192,7,4,7,99,0];
        let rom: Vec<String> = rom.into_iter().map(|x| x.to_string()).collect();
        let rom: String = rom.join(",");
        let file = &String::from(&rom);
        let input = get_input(file);
    

        let mut vm = intvm::create_vm(&input);
        vm.run();

        let output: String = vm.output.into_iter().map(|x| x.to_string()).collect::<Vec<String>>().join(",");
        
        assert_eq!(
            16,
            output.len()
        );
    }

    #[test]
    fn test_part01_3rd() {
        let rom: Vec<i64> = vec![104,1125899906842624,99];
        let rom: Vec<String> = rom.into_iter().map(|x| x.to_string()).collect();
        let rom: String = rom.join(",");
        let file = &String::from(&rom);
        let input = get_input(file);
    

        let mut vm = intvm::create_vm(&input);
        vm.run();

        let output: String = vm.output.into_iter().map(|x| x.to_string()).collect::<Vec<String>>().join(",");
        
        assert_eq!(
            "1125899906842624",
            output
        );
    }
}