mod utils;
mod intvm;
use std::fs;
use std::collections::VecDeque;

const INPUT_FILE: &str = "input.txt";

fn main() {
    let input = fs::read_to_string(INPUT_FILE).expect("Error reading input");
    //let input = "03036732577212944063491565474664";
    let input = input.lines();
    let input: Vec<&str> = input.collect::<Vec<&str>>();

    part_01(input.clone());
    part_02(input.clone());
}

#[allow(unused_variables)]
fn part_01(input: Vec<&str>) {
    let base = input.get(0).unwrap().chars().map(|c| c.to_digit(10));
    let base: Vec<Option<u32>> = base.collect();
    
    let mut last_phase: Vec<i64> = Vec::new();

    for b in &base {
        let a = b.unwrap();
        last_phase.push(a as i64);
    }

    for _ in 0..4 {
        let mut phase: Vec<i64> = Vec::new();

        for pos in 0..last_phase.len() {
            let mut pattern = get_pattern(pos);            
            let mut sum = 0;

            for n in &last_phase {
                let p = pattern.pop_front().unwrap();
                sum += n * p;

                pattern.push_back(p);
            }
            sum = sum.to_string().chars().last().unwrap().to_string().parse::<i64>().unwrap();
            phase.push(sum);
        }

        last_phase = phase.clone();
    }

    print!("Result Part 1: ");

    for i in 0..8 {
        print!("{}", last_phase.get(i).unwrap());
    }

    println!();
}

fn get_pattern(pos: usize) -> VecDeque<i64> {
    let mut pattern: VecDeque<i64> = VecDeque::new();
    let pos = pos + 1;

    for _ in 0..pos {
        pattern.push_back(0);
    };

    for _ in 0..pos {
        pattern.push_back(1);
    };

    for _ in 0..pos {
        pattern.push_back(0);
    };

    for _ in 0..pos {
        pattern.push_back(-1);
    };

    let first = pattern.pop_front().unwrap();
    pattern.push_back(first);

    pattern
}

#[allow(unused_variables)]
fn part_02(input: Vec<&str>) {
    let base = input.get(0).unwrap().chars().map(|c| c.to_digit(10));
    let base: Vec<Option<u32>> = base.collect();
        
    let mut last_phase: Vec<i64> = Vec::new();
    
    for _ in 0..10000 {
        for b in &base {
            let a = b.unwrap();
            last_phase.push(a as i64);
        }
    }
    let mut offset: String = String::new();
    
    for o in 0..7 {
        let o = base.get(o).unwrap().unwrap();
        offset.push_str(&o.to_string());
    }
    
    let offset = offset.parse::<usize>().unwrap();

    let mut phase: Vec<i64> = Vec::new();

    for i in offset..last_phase.len() {
        phase.push(*last_phase.get(i).unwrap());
    }

    last_phase = phase.clone();

    for _ in 0..100 {
        let mut phase: Vec<i64> = Vec::new();

        phase.push(last_phase.iter().sum());

        for i in 1..last_phase.len() {
            phase.push(phase.get(i-1).unwrap() - last_phase.get(i-1).unwrap());
        }

        let next_phase = phase.iter().map(|n| n.to_string().chars().last().unwrap().to_string().parse::<i64>().unwrap());
        let next_phase = next_phase.collect::<Vec<i64>>();

        last_phase = next_phase.clone();
    }

    print!("Result Part 2: ");

    for i in 0..8 {
        print!("{}", last_phase.get(i).unwrap());
    }
    
    println!();
}