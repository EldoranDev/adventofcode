#[derive(Debug)]
pub struct Reaction {
    pub outcome: (String, i64),
    pub requirments: Vec<(String, i64)>,
}

impl Reaction {
    pub fn from_input(input: &str) -> Reaction {
        let parts = input.split("=>").map(|e| e.trim()).collect::<Vec<&str>>();

        let reqs = parts.get(0).unwrap().split(",").map(|e| e.trim()).collect::<Vec<&str>>();
        let res = parts.get(1).unwrap().split(" ").collect::<Vec<&str>>();

        let mut requirments: Vec<(String, i64)> = Vec::new();

        for req in &reqs {
            let split = req.split(" ").collect::<Vec<&str>>();

            requirments.push((
                String::from(*split.get(1).unwrap()),
                split.get(0).unwrap().parse::<i64>().unwrap(),
            ));
        }

        Reaction {
            outcome: (String::from(*res.get(1).unwrap()), (*res.get(0).unwrap()).parse::<i64>().unwrap()),
            requirments,
        }
    }
}