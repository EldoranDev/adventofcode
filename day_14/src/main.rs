mod utils;
mod intvm;
mod reaction;

use reaction::Reaction;
use std::fs;
use std::collections::{HashMap, VecDeque};

const INPUT_FILE: &str = "input.txt";

fn main() {
    let input = fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input =
"10 ORE => 10 A
1 ORE => 1 B
7 A, 1 B => 1 C
7 A, 1 C => 1 D
7 A, 1 D => 1 E
7 A, 1 E => 1 FUEL";

    let input = input.lines();
    let input: Vec<&str> = input.collect::<Vec<&str>>();

    part_01(input.clone());
    part_02(input.clone());
}

#[allow(unused_variables)]
fn part_01(input: Vec<&str>) { 
    let mut recipies: HashMap<String, Reaction> = HashMap::new();

    let mut requirments: VecDeque<(String, i64)> = VecDeque::new();
    let mut produced: HashMap<String, i64> = HashMap::new();
    let mut leftovers: HashMap<String, i64> = HashMap::new();

    for line in input {
        let reaction = Reaction::from_input(line);

        recipies.insert(reaction.outcome.0.clone(), reaction);
    }

    requirments.push_back((String::from("FUEL"), 1));

    while requirments.len() > 0 {
        let req = requirments.pop_front().unwrap();

        println!("NEED: {}x{}", req.0, req.1);

        let reaction: &Reaction = recipies.get(&req.0).unwrap();
        let left = leftovers.get(&req.0).or(Some(&0)).unwrap();

        let need = (req.1 - left) / reaction.outcome.1;
        println!("Need {} reactions for this", need);
        println!("{:?}", reaction);

        for r in &reaction.requirments {
            requirments.push_back((r.0.clone(), r.1 * need));
        }
    }

    println!("{:?}", requirments);
    
    println!("Result of Part 1");
}

#[allow(unused_variables)]
fn part_02(input: Vec<&str>) {
    println!("Result of Part 2");
}