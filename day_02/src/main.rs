use std::fs;
mod utils;
mod intvm;

const INPUT_FILE: &str = "input.txt";

fn main() {
    let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
    let input = get_input(file);

    println!("Result Part 1: {}", part_01(&input));
    println!("Result Part 1: {}", part_02(&input));
}

fn get_input(input: &String) -> Vec<&str> {
    let codes = input.split(",");
    codes.collect::<Vec<&str>>()
}

fn part_01(input: &Vec<&str>) -> i64{

    let mut vm = intvm::create_vm(input);

    vm.set_mem(1, 12);
    vm.set_mem(2, 2);

    let output = vm.run();
    
    output
}

fn part_02(input: &Vec<&str>) -> i64 {
    let mut vm = intvm::create_vm(input);

    for noun in 0..100 {
        for verb in 0..100 {
            vm.reset();

            vm.set_mem(1, noun);
            vm.set_mem(2, verb);

            if vm.run() == 19690720 {
                return (100 * noun) + verb;
            }
        }
    }

    0
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_part01()  {
        let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
        let input = get_input(file);
    
        assert_eq!(part_01(&input), 6568671);
    }

    #[test]
    fn test_sample0101() {
        let file = &String::from("1,0,0,0,99");
        let input = get_input(file);

        let mut vm = intvm::create_vm(&input);

        assert_eq!(vm.run(), 2)
    }

    #[test]
    fn test_sample0102() {
        let file = &String::from("2,3,0,3,99");
        let input = get_input(file);

        let mut vm = intvm::create_vm(&input);

        assert_eq!(vm.run(), 2)
    }

    #[test]
    fn test_sample0103() {
        let file = &String::from("2,4,4,5,99,0");
        let input = get_input(file);

        let mut vm = intvm::create_vm(&input);

        assert_eq!(vm.run(), 2)
    }

    #[test]
    fn test_sample0104() {
        let file = &String::from("1,1,1,4,99,5,6,0,99");
        let input = get_input(file);

        let mut vm = intvm::create_vm(&input);

        assert_eq!(vm.run(), 30)
    }

    #[test]
    fn test_part02()  {
        let file = &fs::read_to_string(INPUT_FILE).expect("Error reading input");
        let input = get_input(file);
    
        assert_eq!(part_02(&input), 3951);
    }
}